import 'package:flutter/material.dart';

class Example3 extends StatelessWidget {
  Example3({Key? key}) : super(key: key);

  final titles = ['Coffee', 'Berry', 'Milk two', 'Caramel',
    'Cake', 'Jelly', 'Black Coffe', 'Juice', 'Cookie'];


  final laeding =['asset/images/1.jpg','asset/images/2.jpg',
    'asset/images/3.jpg','asset/images/4.jpg','asset/images/5.jpg','asset/images/6.jpg'
  ,'asset/images/7.jpg','asset/images/8.jpg','asset/images/9.jpg',];

  final subtitle =['coffee and milk', 'berry soda', 'signegur', 'caramel suga',
    'vanila cake', 'jelly fruit', 'black and brown', 'orenge and rose', 'white cookie'];

  
  @override

  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          onPressed: (){
      },
        child: Icon(Icons.favorite),
      ),
      appBar: AppBar(
        title: Text('ListView3'),
      ),
      body: ListView.builder(
        itemCount: titles.length,
          itemBuilder: (context,index){
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(backgroundImage: AssetImage("${laeding[index]}",),radius: 35,),
                  title: Text('${titles[index]}' , style: TextStyle(fontSize: 18),),
                  subtitle: Text('${subtitle[index]}' , style: TextStyle(fontSize: 15),),
                  trailing: Icon(Icons.favorite_border , size: 25,),

                ),

                Divider(thickness: 1,)
              ],
            );

          },
      ),
    );

  }
}
